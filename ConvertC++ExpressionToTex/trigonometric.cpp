#include "trigonometric.h"

int Trigonometric::precedence()
{
    return 6;
}

NodeType Trigonometric::nodeType()
{
    return NodeType::TRIGONOMETRIC;
}

void Trigonometric::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();

    if (precedence < 10 && prec < precedence)
    {
        result += "\\left( ";
    }

    result += "\\";
    int i = 0;

    if (value[0] == 'a')
    {
        result += "arc";
        i++;
    }

    for (int j = 0; j < 3; j++)
    {
        result += value[i + j];
    }

    // Специальный сигнал: возведение в целую степень.
    if (precedence > 10)
    {
        char tmp[10] = {0};
        itoa(precedence - 10, tmp, 10);
        result += "^{ ";
        result += tmp;
        result += " } ";
    }

    result += ' ';
    child->convertNodeToTeXExp(result, prec);

    if (precedence < 10 && prec < precedence)
    {
        result += "\\right) ";
    }
}

void Trigonometric::clear()
{
    child->clear();
    delete this;
}

bool Trigonometric::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Trigonometric* trig = dynamic_cast<Trigonometric*>(other);
        result =
            this->value == trig->value &&
            this->child->compare(trig->child);
    }

    return result;
}
