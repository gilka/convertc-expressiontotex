#include "comparison.h"

int Comparison::precedence()
{
    return 2;
}

NodeType Comparison::nodeType()
{
    return NodeType::COMPARASION;
}

void Comparison::convertNodeToTeXExp(QString& result, int precedence)
{
    // Приоритет операции.
    int prec = this->precedence();

    if (prec < precedence)
    {
        result += "\\left( ";
    }

    // Конвертируем левый дочерний узел.
    leftChild->convertNodeToTeXExp(result, prec);

    // Конвертируем знаки операций.
    if (value == ">=")
    {
        result += "\\ge";
    }
    else if (value == "<=")
    {
        result += "\\le";
    }
    else if (value == "!=")
    {
        result += "\\ne";
    }
    else
    {
        result += value;
    }

    result += ' ';

    rightChild->convertNodeToTeXExp(result, prec);

    if (prec < precedence)
    {
        result += "\\right) ";
    }
}

void Comparison::clear()
{
    leftChild->clear();
    rightChild->clear();
    delete this;
}

bool Comparison::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Comparison* comp = dynamic_cast<Comparison*>(other);
        result =
            this->value == comp->value &&
            this->leftChild->compare(comp->leftChild) &&
            this->rightChild->compare(comp->rightChild);
    }

    return result;
}
