#include "exponent.h"

int Exponent::precedence()
{
    return 8;
}

NodeType Exponent::nodeType()
{
    return NodeType::EXPONENT;
}

void Exponent::convertNodeToTeXExp(QString& result, int precedence)
{
    // Обернуть result в "e^{ }".
    result += "e^{ ";
    child->convertNodeToTeXExp(result, 0);
    result += "} ";
}

void Exponent::clear()
{
    child->clear();
    delete this;
}

bool Exponent::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Exponent* exp = dynamic_cast<Exponent*>(other);
        result = this->child->compare(exp->child);
    }

    return result;
}
