/*!
 *\file arithmetic.h
 *\brief Файл c описанием узла арифметической операции (+, -, *).
 */

#ifndef ARITHMETIC_H
#define ARITHMETIC_H

#include "binaryoperation.h"

/*!
 * \class Arithmetic : public BinaryOperation
 * \brief Класс, описывающий арифметические операции: +, -, *.
 */
class Arithmetic : public BinaryOperation
{
public:
    char value; /**< \brief Значение. */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // ARITHMETIC_H
