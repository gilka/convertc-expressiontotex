#include "treefuncs.h"

#pragma comment(linker, "/STACK:20000000000")

int main(int argc, char *argv[])
{
    try
    {
        QString file(argv[1]);
        QString exp;
        exp = readExpFromFile(file);

        Node* root = buildTree(exp);

        QString result;
        root->convertNodeToTeXExp(result, 0);

        // Создаем файл с расширением tex.
        int index = file.indexOf('.');
        if (index != -1)
        {
            file.replace(++index, 3, "tex");
        }
        else
        {
            file = file + ".tex";
        }

        writeExpToFile(result, file);

        root->clear();
    }
    catch (char* err)
    {
        puts(err);
    }
    catch (const Error& err)
    {
        QTextStream out(stdout);
        out << err.message << err.lexNum;
        return err.code;
    }

    return 0;
}
