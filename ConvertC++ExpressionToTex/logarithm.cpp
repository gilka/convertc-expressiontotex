#include "logarithm.h"

int Logarithm::precedence()
{
    return 6;
}

NodeType Logarithm::nodeType()
{
    return NodeType::LOGARITHM;
}

void Logarithm::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();

    if (prec < precedence)
    {
        result += "\\left( ";
    }

    // Если value == 0, то логарифм натуральный, иначе - десятичный.
    result += (value == 0) ? "\\ln " : "\\lg ";
    child->convertNodeToTeXExp(result, prec);

    if (prec < precedence)
    {
        result += "\\right) ";
    }
}

void Logarithm::clear()
{
    child->clear();
    delete this;
}

bool Logarithm::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Logarithm* log = dynamic_cast<Logarithm*>(other);
        result =
            this->value == log->value &&
            this->child->compare(log->child);
    }

    return result;
}
