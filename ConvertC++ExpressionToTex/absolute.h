/*!
 *\file absolute.h
 *\brief Файл класса модуля.
 */

#ifndef ABSOLUTE_H
#define ABSOLUTE_H

#include "unaryoperation.h"

/*!
 *\class Absolute : public UnaryOperation
 *\brief Класс модуля.
 */
class Absolute : public UnaryOperation
{
public:
    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // ABSOLUTE_H
