﻿#include "testtreefuncs.h"

void TestTreeFuncs::testBuildVariable()
{
    QString str1("variable");
    QString str2("var1");
    QString str3("my_variable");

    Node* res1;
    Node* res2;
    Node* res3;

    res1 = buildTree(str1);
    res2 = buildTree(str2);
    res3 = buildTree(str3);

    Variable* expTree = new Variable();
    expTree->name = "variable";
    QVERIFY(res1->compare(expTree));
    expTree->name = "var1";
    QVERIFY(res2->compare(expTree));
    expTree->name = "my_variable";
    QVERIFY(res3->compare(expTree));
}

void TestTreeFuncs::testBuildNumber()
{
    QString str1("0");
    QString str2("15");
    QString str3("015");
    QString str4("0x15");
    QString str5("1.35");
    QString str6("2e5");
    QString str7("2e-5");

    Node* res1 = buildTree(str1);
    Node* res2 = buildTree(str2);
    Node* res3 = buildTree(str3);
    Node* res4 = buildTree(str4);
    Node* res5 = buildTree(str5);
    Node* res6 = buildTree(str6);
    Node* res7 = buildTree(str7);

    Number* expTree = new Number();
    expTree->value = "0";
    expTree->integer = true;
    expTree->radix = 10;
    QVERIFY(res1->compare(expTree));

    expTree->value = "15";
    expTree->integer = true;
    expTree->radix = 10;
    QVERIFY(res2->compare(expTree));

    expTree->value = "015";
    expTree->integer = true;
    expTree->radix = 8;
    QVERIFY(res3->compare(expTree));

    expTree->value = "0x15";
    expTree->integer = true;
    expTree->radix = 16;
    QVERIFY(res4->compare(expTree));

    expTree->value = "1.35";
    expTree->integer = false;
    expTree->radix = 0;
    expTree->exponential = false;
    QVERIFY(res5->compare(expTree));

    expTree->value = "2e5";
    expTree->integer = false;
    expTree->radix = 0;
    expTree->exponential = true;
    QVERIFY(res6->compare(expTree));

    expTree->value = "2e-5";
    expTree->integer = false;
    expTree->radix = 0;
    expTree->exponential = true;
    QVERIFY(res7->compare(expTree));
}

void TestTreeFuncs::testBuildArithmetic()
{
    QString str1("a b +");
    QString str2("a b -");
    QString str3("a b *");
    QString str4("a b /");
    QString str5("a --");
    QString str6("a ++");
    QString str7("a (--)");
    QString str8("a (++)");
    QString str9("a (-)");

    Node* res1 = buildTree(str1);
    Node* res2 = buildTree(str2);
    Node* res3 = buildTree(str3);
    Node* res4 = buildTree(str4);
    Node* res5 = buildTree(str5);
    Node* res6 = buildTree(str6);
    Node* res7 = buildTree(str7);
    Node* res8 = buildTree(str8);
    Node* res9 = buildTree(str9);

    Variable* a = new Variable();
    a->name = "a";
    Variable* b = new Variable();
    b->name = "b";

    Number* one = new Number();
    one->value = "1";
    one->integer = true;
    one->radix = 10;

    Arithmetic* expTree = new Arithmetic();
    expTree->value = '+';
    expTree->leftChild = a;
    expTree->rightChild = b;
    QVERIFY(res1->compare(expTree));

    expTree->value = '-';
    QVERIFY(res2->compare(expTree));

    expTree->value = '*';
    QVERIFY(res3->compare(expTree));

    Divide* expTree1 = new Divide();
    expTree1->leftChild = a;
    expTree1->rightChild = b;

    QVERIFY(res4->compare(expTree1));
    QVERIFY(res5->compare(a));
    QVERIFY(res6->compare(a));

    expTree->value = '-';
    expTree->rightChild = one;
    QVERIFY(res7->compare(expTree));

    expTree->value = '+';
    QVERIFY(res8->compare(expTree));

    Sign* expTree2 = new Sign();
    expTree2->child = a;
    QVERIFY(res9->compare(expTree2));
}

void TestTreeFuncs::testBuildCompare()
{
    QString str1("a b >");
    QString str2("a b >=");
    QString str3("a b <");
    QString str4("a b <=");
    QString str5("a b ==");
    QString str6("a b =");
    QString str7("a b !=");

    Node* res1 = buildTree(str1);
    Node* res2 = buildTree(str2);
    Node* res3 = buildTree(str3);
    Node* res4 = buildTree(str4);
    Node* res5 = buildTree(str5);
    Node* res6 = buildTree(str6);
    Node* res7 = buildTree(str7);

    Variable* a = new Variable();
    a->name = "a";
    Variable* b = new Variable();
    b->name = "b";

    Comparison* expTree = new Comparison();
    expTree->leftChild = a;
    expTree->rightChild = b;

    expTree->value = ">";
    QVERIFY(res1->compare(expTree));

    expTree->value = ">=";
    QVERIFY(res2->compare(expTree));

    expTree->value = "<";
    QVERIFY(res3->compare(expTree));

    expTree->value = "<=";
    QVERIFY(res4->compare(expTree));

    expTree->value = "=";
    QVERIFY(res5->compare(expTree));

    expTree->value = "=";
    QVERIFY(res6->compare(expTree));

    expTree->value = "!=";
    QVERIFY(res7->compare(expTree));
}

void TestTreeFuncs::testBuildLogic()
{
    QString str1("a b &&"), str2("a b ||"), str3("a !");
    Node* res1 = buildTree(str1);
    Node* res2 = buildTree(str2);
    Node* res3 = buildTree(str3);

    Variable* a = new Variable();
    a->name = "a";
    Variable* b = new Variable();
    b->name = "b";

    Logic* expTree1 = new Logic();
    expTree1->leftChild = a;
    expTree1->rightChild = b;

    expTree1->value = "&&";
    QVERIFY(res1->compare(expTree1));

    expTree1->value = "||";
    QVERIFY(res2->compare(expTree1));

    Not* expTree2 = new Not();
    expTree2->child = a;
    QVERIFY(res3->compare(expTree2));
}

void TestTreeFuncs::testBuildMath()
{
    QString str1("a b pow()");
    QString str2("a abs()");
    QString str3("a fabs()");
    QString str4("a sqrt()");
    QString str5("a cbrt()");
    QString str6("a floor()");
    QString str7("a ceil()");
    QString str8("a exp()");
    QString str9("a log()");
    QString str10("a log10()");
    QString str11("a acos()");
    QString str12("a asin()");
    QString str13("a atan()");
    QString str14("a cos()");
    QString str15("a sin()");
    QString str16("a tan()");

    Node* res1 = buildTree(str1);
    Node* res2 = buildTree(str2);
    Node* res3 = buildTree(str3);
    Node* res4 = buildTree(str4);
    Node* res5 = buildTree(str5);
    Node* res6 = buildTree(str6);
    Node* res7 = buildTree(str7);
    Node* res8 = buildTree(str8);
    Node* res9 = buildTree(str9);
    Node* res10 = buildTree(str10);
    Node* res11 = buildTree(str11);
    Node* res12 = buildTree(str12);
    Node* res13 = buildTree(str13);
    Node* res14 = buildTree(str14);
    Node* res15 = buildTree(str15);
    Node* res16 = buildTree(str16);

    Variable* a = new Variable();
    a->name = "a";
    Variable* b = new Variable();
    b->name = "b";

    Power* exp1 = new Power();
    exp1->leftChild = a;
    exp1->rightChild = b;
    QVERIFY(res1->compare(exp1));

    Absolute* exp2 = new Absolute();
    exp2->child = a;
    QVERIFY(res2->compare(exp2));
    QVERIFY(res3->compare(exp2));

    Root* exp3 = new Root();
    exp3->child = a;
    exp3->value = 2;
    QVERIFY(res4->compare(exp3));
    exp3->value = 3;
    QVERIFY(res5->compare(exp3));

    Round* exp4 = new Round();
    exp4->child = a;
    exp4->value = "floor()";
    QVERIFY(res6->compare(exp4));
    exp4->value = "ceil()";
    QVERIFY(res7->compare(exp4));

    Exponent* exp5 = new Exponent();
    exp5->child = a;
    QVERIFY(res8->compare(exp5));

    Logarithm* exp6 = new Logarithm();
    exp6->child = a;
    exp6->value = 0;
    QVERIFY(res9->compare(exp6));
    exp6->value = 10;
    QVERIFY(res10->compare(exp6));

    Trigonometric* exp7 = new Trigonometric();
    exp7->child = a;
    exp7->value = "acos()";
    QVERIFY(res11->compare(exp7));
    exp7->value = "asin()";
    QVERIFY(res12->compare(exp7));
    exp7->value = "atan()";
    QVERIFY(res13->compare(exp7));
    exp7->value = "cos()";
    QVERIFY(res14->compare(exp7));
    exp7->value = "sin()";
    QVERIFY(res15->compare(exp7));
    exp7->value = "tan()";
    QVERIFY(res16->compare(exp7));
}

void TestTreeFuncs::testBuildComplex()
{
    QString str("a 3 pow() b 3 pow() +");
    Node* res = buildTree(str);

    Variable* a = new Variable();
    a->name = "a";

    Variable* b = new Variable();
    b->name = "b";

    Number* n1 = new Number();
    n1->value = "3";
    n1->integer = true;
    n1->radix = 10;

    Number* n2 = new Number();
    n2->value = "3";
    n2->integer = true;
    n2->radix = 10;

    Power* pow1 = new Power();
    pow1->leftChild = a;
    pow1->rightChild = n1;

    Power* pow2 = new Power();
    pow2->leftChild = b;
    pow2->rightChild = n2;

    Arithmetic* expTree = new Arithmetic();
    expTree->value = '+';
    expTree->leftChild = pow1;
    expTree->rightChild = pow2;

    QVERIFY(res->compare(expTree));
}

void TestTreeFuncs::testConvertVariable()
{
    QString str;
    QString res;

    str = "variable";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "variable ");

    str = "Var1";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "Var_1 ");

    str = "My_variable";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "My\\_variable ");
}

void TestTreeFuncs::testConvertNumber()
{
    QString str;
    QString res;

    str = "15";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "15 ");

    str = "015";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "15_8 ");

    str = "0x15";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "15_16 ");

    str = "1.35";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "1.35 ");

    str = "2e5";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "2 \\cdot 10^{5} ");

    str = "2e-5";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "2 \\cdot 10^{-5} ");
}

void TestTreeFuncs::testConvertArithmetic()
{
    QString str;
    QString res;

    str = "a b +";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a + b ");

    str = "a b -";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a - b ");

    str = "a b *";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\cdot b ");

    str = "a (-)";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "- a ");
}

void TestTreeFuncs::testConvertDivision()
{
    QString str;
    QString res;

    str = "a b /";
    Node * tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a / b ");

    str = "a b + c /";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\frac{a + b }{c } ");

    str = "a b c + /";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\frac{a }{b + c } ");
}

void TestTreeFuncs::testConvertComparison()
{
    QString str;
    QString res;

    str = "a b >";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a > b ");

    str = "a b <";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a < b ");

    str = "a b >=";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\ge b ");

    str = "a b <=";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\le b ");

    str = "a b =";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a = b ");

    str = "a b !=";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\ne b ");
}

void TestTreeFuncs::testConvertLogic()
{
    QString str;
    QString res;

    str = "a b &&";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\wedge b ");

    str = "a b ||";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\vee b ");

    str = "a !";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\overline{ a } ");
}

void TestTreeFuncs::testConvertPowerRootExp()
{
    QString str;
    QString res;

    str = "a b pow()";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a ^{ b } ");

    str = "a 0.5 pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt{ a } ");

    str = "a 0.25 pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt[4]{ a } ");

    str = "a 1 5 / pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt[5 ]{ a } ");

    str = "a sqrt()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt{ a } ");

    str = "a cbrt()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt[3]{ a } ");

    str = "a exp()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "e^{ a } ");
}

void TestTreeFuncs::testConvertAbsoluteRound()
{
    QString str;
    QString res;

    str = "a abs()";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "| a | ");

    str = "a floor()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\lfloor a \\rfloor ");

    str = "a ceil()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\lceil a \\rceil ");
}

void TestTreeFuncs::testConvertLogarithmTrigonometric()
{
    QString str;
    QString res;

    str = "a log()";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\ln a ");

    str = "a log10()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\lg a ");

    str = "a acos()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\arccos a ");

    str = "a asin()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\arcsin a ");

    str = "a atan()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\arctan a ");

    str = "a cos()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\cos a ");

    str = "a sin()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin a ");

    str = "a tan()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\tan a ");
}

void TestTreeFuncs::testConvertComplex_block1()
{
    QString str;
    QString res;

    str = "a b + c *";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\left( a + b \\right) \\cdot c ");

    str = "a b * c +";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\cdot b + c ");

    str = "a b + c <";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a + b < c ");

    str = "a b > c &&";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a > b \\wedge c ");

    str = "a b || c &&";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\left( a \\vee b \\right) \\wedge c ");

    str = "a b && c ||";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a \\wedge b \\vee c ");

    str = "a b + !";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\overline{ \\left( a + b \\right) } ");

    str = "a b < !";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\overline{ \\left( a < b \\right) } ");

    str = "a b && !";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\overline{ \\left( a \\wedge b \\right) } ");

    str = "a ! b ! &&";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\overline{ a } \\wedge \\overline{ b } ");
}

void TestTreeFuncs::testConvertComplex_block2()
{
    QString str;
    QString res;

    str = "a b pow() sqrt()";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt{ a ^{ b } } ");

    str = "a sqrt() b pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt{ a } ^{ b } ");

    str = "a b + sqrt()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sqrt{ a + b } ");

    str = "a b + 3 pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\left( a + b \\right) ^{ 3 } ");

    str = "2 a b + pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "2 ^{ a + b } ");

    str = "a 3 pow() b 3 pow() +";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "a ^{ 3 } + b ^{ 3 } ");

    str = "x 3 + log()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\ln \\left( x + 3 \\right) ");

    str = "x asin() sin()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin \\arcsin x ");

    str = "x log() sin()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin \\ln x ");

    str = "x sin() log()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\ln \\sin x ");

    str = "x exp() log()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\ln e^{ x } ");

    str = "x y + sin()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin \\left( x + y \\right) ");
}

void TestTreeFuncs::testConvertComplex_block3()
{
    QString str;
    QString res;

    str = "x sqrt() sin()";
    Node* tree = buildTree(str);
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin \\sqrt{ x } ");

    str = "x 2 pow() sin()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin x ^{ 2 } ");

    str = "x sin() 2 pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin^{ 2 }  x ");

    str = "x sin() a b + pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\left( \\sin x \\right) ^{ a + b } ");

    str = "x sin() 2 pow() x cos() 2 pow() + 1 =";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin^{ 2 }  x + \\cos^{ 2 }  x = 1 ");

    str = "1 1 (-) +";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "1 + \\left( - 1 \\right) ");

    str = "a b + (-)";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "- \\left( a + b \\right) ");

    str = "x 3 pow() (-)";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "- x ^{ 3 } ");

    str = "x (-) 3 pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\left( - x \\right) ^{ 3 } ");

    str = "x 3 (-) pow()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "x ^{ - 3 } ");

    str = "3 (-) x /";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\frac{- 3 }{x } ");

    str = "x 3 / (-)";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "- x / 3 ");

    str = "x 3 y + / (-)";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "- \\frac{x }{3 + y } ");

    str = "x sin() (-)";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "- \\sin x ");

    str = "x (-) sin()";
    tree = buildTree(str);
    res.clear();
    tree->convertNodeToTeXExp(res, 0);
    QVERIFY(res == "\\sin \\left( - x \\right) ");
}
