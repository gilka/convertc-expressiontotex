#include "power.h"

int Power::precedence()
{
    return 8;
}

NodeType Power::nodeType()
{
    return NodeType::POWER;
}

void Power::convertNodeToTeXExp(QString& result, int precedence)
{
    int prec = this->precedence();
    bool special = false;

    // Специальный случай: тригонометрическая функция в целой степени.
    if (leftChild->nodeType() == NodeType::TRIGONOMETRIC &&
        rightChild->nodeType() == NodeType::NUMBER)
    {
        Number* tmp = dynamic_cast<Number*>(rightChild);

        if (tmp->integer && tmp->radix == 10)
        {
            // Вместо приоритета передаем специальный код - степень.
            leftChild->convertNodeToTeXExp(result, 10 + tmp->value.toInt());
            special = true;
        }
    }
    // Специальный случай: показатель степени равен 0.5 или 0.25.
    else if (rightChild->nodeType() == NodeType::NUMBER)
    {
        Number* tmp = dynamic_cast<Number*>(rightChild);

        if (tmp->value == "0.5" || tmp->value == "0.25")
        {
            result += "\\sqrt";

            if (tmp->value == "0.25")
            {
                result += "[4]";
            }

            result += "{ ";
            leftChild->convertNodeToTeXExp(result, prec);
            result += "} ";
            special = true;
        }
    }
    // Специальный случай: показатель степени - 1/x, где х - целое число.
    else if (rightChild->nodeType() == NodeType::DIVIDE)
    {
        Divide* tmp = dynamic_cast<Divide*>(rightChild);

        if (tmp->leftChild->nodeType() == NodeType::NUMBER &&
            tmp->rightChild->nodeType() == NodeType::NUMBER)
        {
            Number* tmp1 = dynamic_cast<Number*>(tmp->leftChild);
            Number* tmp2 = dynamic_cast<Number*>(tmp->rightChild);

            if (tmp1->value == "1" && tmp2->integer)
            {
                result += "\\sqrt[";
                tmp2->convertNodeToTeXExp(result, 0);
                result += "]{ ";
                leftChild->convertNodeToTeXExp(result, prec);
                result += "} ";
                special = true;
            }
        }
    }

    if (!special)
    {
        if (prec < precedence)
        {
            result += "\\left( ";
        }

        leftChild->convertNodeToTeXExp(result, prec);
        result += "^{ ";
        rightChild->convertNodeToTeXExp(result, 0);
        result += "} ";

        if (prec < precedence)
        {
            result += "\\right) ";
        }
    }
}

void Power::clear()
{
    leftChild->clear();
    rightChild->clear();
    delete this;
}

bool Power::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Power* p = dynamic_cast<Power*>(other);
        result =
            this->leftChild->compare(p->leftChild) &&
            this->rightChild->compare(p->rightChild);
    }

    return result;
}
