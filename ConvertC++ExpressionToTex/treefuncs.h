/*!
 *\file treefuncs.h
 *\brief Файл основных функций для работы с деревом.
 */

#ifndef TREEFUNCS_H
#define TREEFUNCS_H

#include <functional>
#include <QMap>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include "node.h"
#include "arithmetic.h"
#include "divide.h"
#include "number.h"
#include "sign.h"
#include "comparison.h"
#include "logic.h"
#include "not.h"
#include "power.h"
#include "absolute.h"
#include "root.h"
#include "round.h"
#include "exponent.h"
#include "logarithm.h"
#include "trigonometric.h"
#include "variable.h"
#include "error.h"

Node* plusMinusMultiplyHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* divisionHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* postfixIncrementDecrementHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* prefixIncrementDecrementHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* unaryMinusHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* comparasionHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* logicHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* notHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* powerHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* moduleHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* squareRootHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* floorCeilHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* exponentHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* logarithmHandler(const QString operation, const QStringList& lexems, int& lexNum);
Node* trigonometricHandler(const QString operation, const QStringList& lexems, int& lexNum);

/*!
 *\brief Функция добавления узла с текущей лексемой к дереву.
 *\param[in] lexems Массив лексем.
 *\param[in] lexNum Номер обрабатываемой лексемы.
 *\return Указатель на созданный узел.
 *\throw Error Описание ошибки.
 */
Node* addNodeFromStr(const QStringList& lexems, int& lexNum) throw (const Error&);

/*!
*\brief Функция построение дерева по его описанию в обратной польской нотации.
*\param[in] str Строка с описанием дерева.
*\return Указатель на корень дерева.
*\throw (...) Либо строка с сообщением об ошибке, либо Error с описанием ошибки.
*/
Node* buildTree(const QString& str) throw (...);

/*!
 *\brief Функция считывания строки с описанием дерева из файла.
 *\param[in] filename Имя файла с описанием дерева.
 *\return Строка с описанием дерева.
 *\throw char* Сообщение об ошибке.
 */
QString readExpFromFile(const QString& filename) throw (char*);

/*!
 *\brief Функция записи строки с выражением ТеХ в файл.
 *\param[in] exp Строка с выражением ТеХ.
 *\param[in] filename Имя файла для записи.
 *\throw char* Сообщение об ошибке.
 */
void writeExpToFile(const QString& exp, const QString& filename) throw (char*);

#endif // TREEFUNCS_H
