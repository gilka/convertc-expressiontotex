/*!
 *\file exponent.h
 *\brief Файл класса экспоненты.
 */

#ifndef EXPONENT_H
#define EXPONENT_H

#include "unaryoperation.h"

/*!
 *\class Exponent : public UnaryOperation
 *\brief Класс экспоненты.
 */
class Exponent : public UnaryOperation
{
public:
    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // EXPONENT_H
