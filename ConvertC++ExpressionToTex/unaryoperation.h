/*!
 *\file unaryoperation.h
 *\brief Файл c описанием узла унарной операции.
 */

#ifndef UNARYOPERATION_H
#define UNARYOPERATION_H

#include "node.h"

/*!
 *\class UnaryOperation : public Node
 *\brief Абстрактный класс, описывающий унарные операции.
 */
class UnaryOperation : public Node
{
public:
    Node* child; /**< \brief Указатель на операнд. */
};

#endif // UNARYOPERATION_H
