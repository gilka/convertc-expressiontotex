/*!
 *\file logic.h
 *\brief Файл, описывающий логические операции и/или.
 */

#ifndef LOGIC_H
#define LOGIC_H

#include "binaryoperation.h"

/*!
 *\class Logic : public BinaryOperation
 *\brief Класс, описывающий логические операции и/или.
 */
class Logic : public BinaryOperation
{
public:
    QString value; /**< \brief Значение. */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // LOGIC_H
