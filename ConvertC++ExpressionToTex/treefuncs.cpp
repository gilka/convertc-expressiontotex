#include "treefuncs.h"

QMap<QString, std::function<Node*(const QString& operations, const QStringList& lexems, int& lexNum)>> operationHandlers =
{
    {"+", plusMinusMultiplyHandler},
    {"-", plusMinusMultiplyHandler},
    {"*", plusMinusMultiplyHandler},
    {"/", divisionHandler},
    {"++", postfixIncrementDecrementHandler},
    {"--", postfixIncrementDecrementHandler},
    {"(++)", prefixIncrementDecrementHandler},
    {"(--)", prefixIncrementDecrementHandler},
    {"(-)", unaryMinusHandler},
    {">", comparasionHandler},
    {">=", comparasionHandler},
    {"<", comparasionHandler},
    {"<=", comparasionHandler},
    {"==", comparasionHandler},
    {"=", comparasionHandler},
    {"!=", comparasionHandler},
    {"&&", logicHandler},
    {"||", logicHandler},
    {"!", notHandler},
    {"pow()", powerHandler},
    {"abs()", moduleHandler},
    {"fabs()", moduleHandler},
    {"sqrt()", squareRootHandler},
    {"cbrt()", squareRootHandler},
    {"floor()", floorCeilHandler},
    {"ceil()", floorCeilHandler},
    {"exp()", exponentHandler},
    {"log()", logarithmHandler},
    {"log10()", logarithmHandler},
    {"acos()", trigonometricHandler},
    {"asin()", trigonometricHandler},
    {"atan()", trigonometricHandler},
    {"cos()", trigonometricHandler},
    {"sin()", trigonometricHandler},
    {"tan()", trigonometricHandler}
};

Node* plusMinusMultiplyHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 2)
    {
        throw lexNum;
    }

    Arithmetic* node = new Arithmetic();
    node->value = operation[0].toLatin1();
    node->rightChild = addNodeFromStr(lexems, --lexNum);
    node->leftChild = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* divisionHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 2)
    {
        throw lexNum;
    }

    Divide* node = new Divide();
    node->rightChild = addNodeFromStr(lexems, --lexNum);
    node->leftChild = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* postfixIncrementDecrementHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    // Постфиксные инкремент и декремент пропускаем.
    Node* node = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* prefixIncrementDecrementHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Arithmetic* node = new Arithmetic();
    node->value = operation == "(++)" ? '+' : '-';
    Number* rightChild = new Number();
    rightChild->value = "1";
    rightChild->integer = true;
    rightChild->radix = 10;
    node->rightChild = rightChild;
    node->leftChild = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* unaryMinusHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Sign* node = new Sign();
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* comparasionHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 2)
    {
        throw lexNum;
    }

    Comparison* node = new Comparison();
    node->value = operation == "==" ? "=" : operation;
    node->rightChild = addNodeFromStr(lexems, --lexNum);
    node->leftChild = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* logicHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 2)
    {
        throw lexNum;
    }

    Logic* node = new Logic();
    node->value = operation;
    node->rightChild = addNodeFromStr(lexems, --lexNum);
    node->leftChild = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* notHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Not* node = new Not();
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* powerHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 2)
    {
        throw lexNum;
    }

    Power* node = new Power();
    node->rightChild = addNodeFromStr(lexems, --lexNum);
    node->leftChild = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* moduleHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Absolute* node = new Absolute();
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* squareRootHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Root* node = new Root();
    node->value = (operation == "sqrt()") ? 2 : 3;
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* floorCeilHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Round* node = new Round();
    node->value = operation;
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* exponentHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Exponent* node = new Exponent();
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* logarithmHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Logarithm* node = new Logarithm();
    node->value = operation == "log()" ? 0 : 10;
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* trigonometricHandler(const QString operation, const QStringList& lexems, int& lexNum)
{
    if (lexNum < 1)
    {
        throw lexNum;
    }

    Trigonometric* node = new Trigonometric();
    node->value = operation;
    node->child = addNodeFromStr(lexems, --lexNum);
    return node;
}

Node* addNodeFromStr(const QStringList& lexems, int& lexNum) throw (const Error&)
{
    Node* result = nullptr;
    QString tmp = lexems[lexNum];

    try
    {
        if (operationHandlers.contains(tmp))
        {
            result = operationHandlers[tmp](tmp, lexems, lexNum);
        }
        // Начинается с цифры - значит число.
        else if (tmp[0].isDigit())
        {
            bool OK = true;
            Error err;
            Number* node = new Number();
            node->value = tmp;

            // Если число - 0.
            if (tmp == "0")
            {
                node->integer = true;
                node->radix = 10;
            }
            // Если число содержит точку.
            else if (tmp.contains('.'))
            {
                tmp.toDouble(&OK);

                if (!OK)
                {
                    err.message = "Incorrect fractional number: lexem #";
                }

                node->integer = false;

                // И букву е.
                if (tmp.contains('e'))
                {
                    node->exponential = true;
                }
            }
            // Если начинается с нуля, то это 8-ричная или 16-ричная форма записи.
            else if (tmp[0] == '0')
            {
                node->radix = (tmp.size() > 1 && tmp[1] == 'x') ? 16 : 8;
                tmp.toInt(&OK, node->radix);

                if (!OK)
                {
                    if (node->radix == 8)
                    {
                        err.message = "Incorrect octal notation of number: lexem #";
                    }
                    else if (node->radix == 16)
                    {
                        err.message = "Incorrect hexadecimal notation of number: lexem #";
                    }
                }

                node->integer = true;
            }
            // Содержит букву е, но начинается не с 0х.
            else if (tmp.contains('e'))
            {
                tmp.toDouble(&OK);

                if (!OK)
                {
                    err.message = "Incorrect fractional number: lexem #";
                }

                node->integer = false;
                node->exponential = true;
            }
            // Целое число в десятичной форме записи.
            else
            {
                tmp.toInt(&OK);

                if (!OK)
                {
                    err.message = "Incorrect number: lexem #";
                }

                node->integer = true;
                node->radix = 10;
            }

            if (!OK)
            {
                err.lexNum = lexNum;
                // Код ошибки: ошибка в записи числа.
                err.code = 4;
                throw err;
            }

            result = node;
        }
        // Начинается с буквы - переменная.
        else if (tmp[0].isLetter())
        {
            bool correct = true;
            int size = tmp.size();

            for (int i = 0; i < size && correct; i++)
            {
                // Может содержать только буквы, цифры и символ нижнего подчеркивания.
                if (!(tmp[i].isLetter() || tmp[i].isDigit() || tmp[i] == '_'))
                {
                    correct = false;
                }
            }

            if (!correct)
            {
                Error err;
                err.message = "Invalid symbol in the name of variable: lexem #";
                err.lexNum = lexNum;

                // Код ошибки: неправильное имя переменной.
                err.code = 3;
                throw err;
            }

            Variable* node = new Variable();
            node->name = tmp;
            result = node;
        }
        // Неизвестный тип лексемы.
        else
        {
            Error err;
            err.message = "Invalid operation: lexem #";
            err.lexNum = lexNum;
            // Код ошибки: неизветсная операция.
            err.code = 2;
            throw err;
        }
    }
    // Ловим недостаток операндов.
    catch (int num)
    {
        Error err;
        err.message = "Lack of operands for operation: lexem #";
        err.lexNum = num;
        // Код ошибки: неверное количество операндов.
        err.code = 1;
        throw err;
    }

    return result;
}

Node* buildTree(const QString& str) throw (...)
{
    // Пустая строка.
    if (str.length() == 0)
    {
        throw "Void string";
    }

    QStringList lexems = str.split(" ");
    int size = lexems.size();
    Node* root = addNodeFromStr(lexems, --size);

    // Неверное количество операндов.
    if (size >= 1)
    {
        Error err;
        err.lexNum = size;
        err.message = "Redundant operands: lexems from 1 to ";
        // Код ошибки: неверное количество операндов.
        err.code = 1;
        throw err;
    }

    return root;
}

QString readExpFromFile(const QString& filename) throw (char*)
{
    QString result;
    QFile file(filename);

    // Удалось открыть файл.
    if (file.open(QIODevice::ReadOnly))
    {
        QTextStream input(&file);
        result = input.readAll();
    }
    // Не удалось открыть файл.
    else
    {
        throw "Error: cannot open file for read";
    }

    return result;
}

void writeExpToFile(const QString& exp, const QString& filename) throw (char*)
{
    QFile file(filename);

    // Удалось открыть файл.
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream output(&file);
        output << exp;
    }
    // Не удалось открыть файл.
    else
    {
        throw "Error: cannot open file for write";
    }
}
