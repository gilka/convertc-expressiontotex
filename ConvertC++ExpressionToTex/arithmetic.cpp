#include "arithmetic.h"

int Arithmetic::precedence()
{
    return value == '*' ? 5 : 4;
}

NodeType Arithmetic::nodeType()
{
    return NodeType::ARITHMETIC;
}

void Arithmetic::convertNodeToTeXExp(QString& result, int precedence)
{
    // Приоритет операции.
    int prec = this->precedence();

    if (prec < precedence)
    {
        result += "\\left( ";
    }

    // Конвертируем левый дочерний узел.
    leftChild->convertNodeToTeXExp(result, prec);

    if (value == '*')
    {
        result += "\\cdot";
    }
    else
    {
        result += value;
    }

    result += ' ';

    rightChild->convertNodeToTeXExp(result, prec + 1);

    // Нужно добавить \right.
    if (prec < precedence)
    {
        result += "\\right) ";
    }
}

void Arithmetic::clear()
{
    leftChild->clear();
    rightChild->clear();
    delete this;
}

bool Arithmetic::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Arithmetic* ar = dynamic_cast<Arithmetic*>(other);
        result =
            this->value == ar->value &&
            this->leftChild->compare(ar->leftChild) &&
            this->rightChild->compare(ar->rightChild);
    }

    return result;
}
