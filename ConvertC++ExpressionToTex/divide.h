/*!
 *\file divide.h
 *\brief Файл операции деления.
 */

#ifndef DIVIDE_H
#define DIVIDE_H

#include "binaryoperation.h"

/*!
 *\class Divide : public BinaryOperation
 *\brief Класс операции деления.
 */
class Divide : public BinaryOperation
{
public:
    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // DIVIDE_H
