#include "number.h"

Number::Number()
{
    integer = exponential = false;
    radix = 0;
}

int Number::precedence()
{
    // У экспоненциальной формы записи приоритет равен умножению.
    return exponential ? 5 : 10;
}

NodeType Number::nodeType()
{
    return NodeType::NUMBER;
}

void Number::convertNodeToTeXExp(QString& result, int precedence)
{
    // Экспоненциальная запись.
    if (exponential)
    {
        // Приоритет операции.
        int prec = this->precedence();

        if (prec < precedence)
        {
            result += "\\left( ";
        }

        int i = 0;
        int size = value.size();
        for (i; value[i] != 'e'; i++)
        {
            result += value[i];
        }

        result += " \\cdot 10^{";

        for (++i; i < size; i++)
        {
            result += value[i];
        }

        result += '}';

        if (prec < precedence)
        {
            result += "\\right ";
        }
    }
    // Обычная запись.
    else
    {
        QString number = value;
        int del = 0;

        if (integer && radix != 10)
        {
            del = (radix == 8) ? 1 : 2;
        }

        number.remove(0, del);
        result += number;

        if (integer && radix != 10)
        {
            result += '_';
            result += QString::number(radix);
        }
    }

    result += ' ';
}

void Number::clear()
{
    delete this;
}

bool Number::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Number* num = dynamic_cast<Number*>(other);
        result =
            this->value == num->value &&
            this->integer == num->integer &&
            this->radix == num->radix &&
            this->exponential == num->exponential;
    }

    return result;
}
