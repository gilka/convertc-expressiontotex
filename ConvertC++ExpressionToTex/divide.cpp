#include "divide.h"

int Divide::precedence()
{
    return 5;
}

NodeType Divide::nodeType()
{
    return NodeType::DIVIDE;
}

void Divide::convertNodeToTeXExp(QString& result, int precedence)
{
    NodeType lType = leftChild->nodeType(), rType = rightChild->nodeType();

    // Деление двух простых операндов.
    if ((lType == NodeType::NUMBER || lType == NodeType::VARIABLE) &&
        (rType == NodeType::NUMBER || rType == NodeType::VARIABLE))
    {
        int prec = this->precedence();

        if (prec < precedence)
        {
            result += "\\left( ";
        }

        leftChild->convertNodeToTeXExp(result, 0);
        result += "/ ";
        rightChild->convertNodeToTeXExp(result, 0);

        if (prec < precedence)
        {
            result += "\\right) ";
        }
    }
    else
    {
        result += "\\frac{";
        leftChild->convertNodeToTeXExp(result, 0);
        result += "}{";
        rightChild->convertNodeToTeXExp(result, 0);
        result += "} ";
    }
}

void Divide::clear()
{
    leftChild->clear();
    rightChild->clear();
    delete this;
}

bool Divide::compare(Node* other)
{
    bool result = this->nodeType() == other->nodeType();

    if (result)
    {
        Divide* divide = dynamic_cast<Divide*>(other);
        result =
            this->leftChild->compare(divide->leftChild) &&
            this->rightChild->compare(divide->rightChild);
    }

    return result;
}
