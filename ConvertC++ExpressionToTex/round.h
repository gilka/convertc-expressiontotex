/*!
 *\file round.h
 *\brief Файл класса округления.
 */

#ifndef ROUND_H
#define ROUND_H

#include "unaryoperation.h"

/*!
 *\class Round : public UnaryOperation
 *\brief Класс округления.
 */
class Round : public UnaryOperation
{
public:
    QString value; /**< \brief Значение. */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // ROUND_H
