/*!
 *\file trigonometric.h
 *\brief Файл класса тригонометрических функций.
 */

#ifndef TRIGONOMETRIC_H
#define TRIGONOMETRIC_H

#include "unaryoperation.h"

/*!
 *\class Trigonometric : public UnaryOperation
 *\brief Класс тригонометрических функций.
 */
class Trigonometric : public UnaryOperation
{
public:
    QString value; /**< \brief Значение. */

    int precedence() override;
    NodeType nodeType() override;
    void convertNodeToTeXExp(QString& result, int precedence) override;
    void clear() override;
    bool compare(Node* other) override;
};

#endif // TRIGONOMETRIC_H
